/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author lenovo
 */
public class Coche
{
    private String motor = "";
    private String carroceria = "";
    private Boolean vidriosElec = false;
    private Boolean aireAcond = false;
    // -------------------------------------------
    public Coche() {
    }
    // -------------------------------------------
    public String getMotor() {
        return this.motor;
    }
    // -------------------------------------------
    public void setMotor(String motor) {
        this.motor = motor;
    }
    // -------------------------------------------
    public String getCarroceria() {
        return this.carroceria;
    }
    // -------------------------------------------
    public void setCarroceria(String carroceria) {
        this.carroceria = carroceria;
    }
    // -------------------------------------------
    public Boolean getVidriosElec() {
        return vidriosElec;
    }
    // -------------------------------------------
    public void setVidriosElec(Boolean elevalunasElec) {
        this.vidriosElec = vidriosElec;
    }
    // -------------------------------------------
    public Boolean getAireAcond() {
        return aireAcond;
    }
    // -------------------------------------------
    public void setAireAcond(Boolean aireAcond) {
        this.aireAcond = aireAcond;
    }
}